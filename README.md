
# Multitool

Multitool is a library for creating simple command line apps using plain Elixir modules. 

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `multitool` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:multitool, "~> 0.1.0"}]
end
```
