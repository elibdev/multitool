defmodule Multitool do
  @moduledoc """
  Multitool is a library for creating simple command line apps
  using plain Elixir modules. 

  ## How to Use Multitool

  Multitool takes an Elixir module and makes a command line app
  where each exported function becomes a subcommand. 
  It uses the module's and functions' documentation to
  provide command-line help, similar to Mix tasks. 

  First you write an Elixir module that follows one rule:

  1. All exported functions should accept a single argument,
  which will be the space-delimited arguments given to the app.
  
  Multitool will make a command line app with a subcommand
  for each function your module exports.

  Multitool automatically defines a few things in your module:

  - `@name` is set to the innermost module's lower-case name.
  `:'Elixir.Math.Sum'` becomes `"sum"`.
  - `main/1` is defined to dispatch  commands to the functions.
  - `help/1` is defined to display help text for the commands 
  extracted from `@moduledoc` and `@doc`.
  - `version/0` is defined to return a string representation
  of the module's `@vsn`.

  ```
  # mix.exs
  def project do
    [
      app: :example,
      version: Example.version
      escript: [main_module: Example],
   ]
  end
  ```

  ```
  # example.ex
  defmodule Example do
    @moduledoc "Perform some simple operations on a word."
    use Multitool

    @doc "reverse the word"
    def reverse([s]), do: IO.puts(String.reverse(s))

    @doc "make the word uppercase"
    def up([s]), do: IO.puts(String.upcase(s))

    @doc "make the word lowercase"
    def down([s]), do: IO.puts(String.downcase(s))

    @doc "slow the word down"
    def slow([s]) do
      String.graphemes(s)
      |> Enum.join(" ")
      |> IO.puts()
    end
  end
  ```
  """
  defmacro __using__(_) do
    quote do
      @vsn "0.1.0"

      # Define a function to expose the module's @vsn.
      def version(), do: @vsn

      @before_compile Multitool
    end
  end

  defmacro __before_compile__(env) do
    vsn = Module.get_attribute(env.module, :vsn)
    {_, moduledoc} = Module.get_attribute(env.module, :moduledoc)
    quote do
      @module __ENV__.module

      @name @module
      |> to_string()
      |> String.split(".")
      |> List.last()
      |> String.downcase()

      # Make text output bright. 
      defp b(s) do
        IO.ANSI.format([:blue, :bright, s])
      end

      # Define a main function that dispatches to the functions.
      def main([]), do: help([])
      def main(["help"]), do: help([])
      def main(["help", cmd]) do
        help([cmd])
      end
      def main([cmd | args]) do
        # Dispatch to the inner functions.
        apply(@module, String.to_atom(cmd), [args])
      end

      @doc """
      Display help text for a command. 
      """
      def help([]) do
        IO.puts("#{@name} v#{unquote(vsn)}\n")

        IO.puts(unquote(moduledoc) <> "\n")

        IO.puts("Available commands:\n")
        @module.__info__(:functions)
        |> Keyword.drop([:main, :version])
        |> Keyword.keys()
        |> Enum.map(fn f -> "#{@name} " <> to_string(f) end)
        |> Enum.join("\n")
        |> (fn s -> IO.puts("""
            #{s}

            Enter "#{@name} help [command]" to get help for [command].
            """)
          end).()
      end
      def help([cmd]) do
        # Get the doc string of the function in the module.
        f = String.to_atom(cmd)
        docs = Code.get_docs(@module, :docs)
        |> Enum.find(fn
          {{^f, _},_,_,_,_} -> true
          _ -> false
        end) |> elem(4)
        IO.puts("\n#{docs}\n")
      end
    end
  end
end

defmodule Example do
  @moduledoc "Perform some simple operations on a word."
  use Multitool

  @doc "reverse the word"
  def reverse([s]), do: IO.puts(String.reverse(s))

  @doc "make the word uppercase"
  def up([s]), do: IO.puts(String.upcase(s))

  @doc "make the word lowercase"
  def down([s]), do: IO.puts(String.downcase(s))

  @doc "slow the word down"
  def slow([s]) do
    String.graphemes(s)
    |> Enum.join(" ")
    |> IO.puts()
  end
end
