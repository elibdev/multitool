defmodule MultitoolTest do
  use ExUnit.Case
  import ExUnit.CaptureIO
  doctest Multitool

  test "app module exports correct functions" do

    assert Example.version == "0.1.0"
    assert Example.__info__(:functions)
    |> Keyword.get(:main) == 1

    assert Example.__info__(:attributes) == [vsn: ["0.1.0"]]

    assert capture_io(fn -> 
      Example.main(["help"]) 
    end) == """
    example v0.1.0

    Perform some simple operations on a word.

    Available commands:

    example down
    example help
    example reverse
    example slow
    example up

    Enter "example help [command]" to get help for [command].

    """
    assert capture_io(fn -> 
      Example.main(["help", "up"])
    end) == """

    make the word uppercase
    
    """
    assert capture_io(fn -> 
      Example.main(["help", "slow"])
    end) == """
    
    slow the word down
    
    """
    assert capture_io(fn -> 
      Example.main(["slow", "hello"])
    end) == "h e l l o\n"
    assert capture_io(fn -> 
      Example.main(["reverse", "hello"])
    end) == "olleh\n"
  end
end
